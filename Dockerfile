ARG DOCKER_SUPER_IMAGE
FROM $DOCKER_SUPER_IMAGE

ARG DOCKER_COMMIT_AUTHOR
LABEL maintainer=$DOCKER_COMMIT_AUTHOR

ARG GUIX_BINARY_ARCH
ARG GUIX_BINARY_VERSION
ARG GUIX_BINARY_GPG_SIGNING_KEY_ID

VOLUME /guix-state-deploy
VOLUME /guix-store-deploy

RUN                                                                                                                     \
  cd /tmp ;                                                                                                             \
  apk add --no-cache gnupg ;                                                                                            \
  wget -q https://ftp.gnu.org/gnu/guix/guix-binary-${GUIX_BINARY_VERSION}.${GUIX_BINARY_ARCH}.tar.xz ;                  \
  wget -q https://ftp.gnu.org/gnu/guix/guix-binary-${GUIX_BINARY_VERSION}.${GUIX_BINARY_ARCH}.tar.xz.sig ;              \
  gpg --keyserver pool.sks-keyservers.net --recv-keys ${GUIX_BINARY_GPG_SIGNING_KEY_ID} ;                               \
  gpg --verify guix-binary-${GUIX_BINARY_VERSION}.${GUIX_BINARY_ARCH}.tar.xz.sig ;                                      \
  tar -xf guix-binary-${GUIX_BINARY_VERSION}.${GUIX_BINARY_ARCH}.tar.xz ;                                               \
  mv var/guix /guix-state-source ;                                                                                      \
  mv gnu/store /guix-store-source ;                                                                                     \
                                                                                                                        \
  rm -f guix-binary-${GUIX_BINARY_VERSION}.${GUIX_BINARY_ARCH}.tar.xz.sig ;                                             \
  rm -f guix-binary-${GUIX_BINARY_VERSION}.${GUIX_BINARY_ARCH}.tar.xz ;

CMD ["sh", "-c", "cp -af /guix-state-source/* /guix-state-deploy && cp -af /guix-store-source/* /guix-store-deploy"]
