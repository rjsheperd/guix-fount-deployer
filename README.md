Guix Fount Deployer
===================

This image fulfills a simple purpose - it deploys a fresh installation of the [GNU Guix](https://guix.gnu.org/) state and store directories `/var/guix` and `/gnu/store`, collectively the [*fount*](https://www.google.com/search?q=fount), at specific mount points in a container for the purpose of provisioning a new Guix
store on storage external to the container, corresponding to the version of Guix specified by the image tag.

Tags
----

* [`1.1.0`](https://ftp.gnu.org/gnu/guix/guix-binary-1.1.0.x86_64-linux.tar.xz)

Usage
-----

Invoke the container as follows:

    docker run --rm --mount type=bind,source=$PWD/state,target=/guix-state-deploy --mount type=bind,source=$PWD/store,target=/guix-store-deploy guix-fount-deployer

When finished, the `$PWD/state` and `$PWD/store` directories will be populated with the contents from the `/var/guix` and `/gnu/store` directories of a fresh Guix installation.
